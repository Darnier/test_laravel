<?php

use Illuminate\Database\Seeder;
use App\Lang;

class LangTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Lang::create([
            'lang' => 'English',
            'lang_code' => 'en',
        ])->create([
            'lang' => 'Ukrainian',
            'lang_code' => 'ua',
        ]);
    }
}
