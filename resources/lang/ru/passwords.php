<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Ваш пароль було скинуто!',
    'sent' => 'Вам надіслано посилання для зміни пароля!',
    'throttled' => 'Зачекайте, не так швидко.',
    'token' => 'Цей токен скидання паролю невірний.',
    'user' => "Користувача з такою електронною поштою не існує.",

];
