<?php

namespace App\Http\Controllers;

use App\Lang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use App\User;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    public function localization($localization)
    {
        Session::put('localization', $localization);

        if (Auth::check()){
            $lang_id = Lang::where('lang_code', $localization)->first('id')->id;
            $user = User::find(Auth::id());
            $user->setAttribute('lang_id', $lang_id);
            $user->save();
        }
        return back();
    }

    public function profile()
    {

        if (Auth::check()){
            $user = User::find(Auth::id());
        } else {
            return redirect()->route('login');
        }
        $referals = User::where('referer', $user->id)->orderBy('created_at', 'desc')->take(6)->get();

        return view('profile')->with('user', $user)->with('referals', $referals);
    }


    public function profileSave(Request $request)
    {
        if (Auth::check()){
            $user = User::find(Auth::id());

            $user->setAttribute('name', $request->get('name'));
            $user->setAttribute('email', $request->get('email'));

            $user->save();
        } else {
            return redirect()->route('login');
        }

        return redirect()->route('profile');
    }
}
