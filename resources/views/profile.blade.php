@extends('layouts.app')

@section('content')
    <div class="profile">
        <div class="container">
            <h3>Редагування профілю</h3>
            <h4><b>Увага!</b> Ці дані будуть використовуватися при доставці вашого замовлення.</h4>
            <form action="{{route('profile_save')}}" method="post" class="checkout_form profile_form">
                @csrf
                <div>
                    <label for="email">{{ __('custom.E-Mail Address') }} : </label>
                    <input type="text" name="email" value="{{$user->email}}">
                </div>
                <div>
                    <label for="name">Ім'я : </label>
                    <input type="text" name="name" value="{{$user->name}}">
                </div>
                <input type="submit" value="Save">
            </form>
            <h1>{{ \Illuminate\Support\Facades\Session::get('locale') }}</h1>
            <h1>Реферальне посилання:</h1>
            <h2>{{ route('ref_register', $user->referal_string)  }}</h2>
            <h1>Зареєстровані за вашим посиланням:</h1>
        @foreach($referals as $ref)
                <h2>{{ $ref->name .' ( '.$ref->email.' ) ' }}</h2>
            @endforeach
        </div>
    </div>
@endsection
