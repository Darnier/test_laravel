<?php

namespace App\Http\Middleware;

use App\Lang;
use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\App;

class Localization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()){
            $user = User::find(Auth::id());
            $lang_code = Lang::find($user->lang_id)->lang_code;
            App::setlocale($lang_code);
        } elseif(Session::has('localization'))
        {
            App::setlocale(Session::get('localization'));
        }
        return $next($request);
    }
}
