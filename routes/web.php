<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@profile')->name('profile');
Route::post('/profile/save', 'HomeController@profileSave')->name('profile_save');

Route::get('register/{ref?}', 'Auth\RegisterController@showRegistrationForm')->name('ref_register');

Route::get('localization/{localization}', 'HomeController@localization')->name('localization');

